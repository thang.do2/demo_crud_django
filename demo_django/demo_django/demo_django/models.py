from django.db import models
# Create your models here.


class ProductModel(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    price = models.FloatField(default=0)
    image = models.CharField(max_length=200, null=True)
    active = models.BooleanField(null=True)
    create_date = models.DateTimeField(null=True)

    class Meta:
        db_table = "product"
