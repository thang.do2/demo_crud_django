from django import forms
from demo_django.models import ProductModel


class Productforms(forms.ModelForm):
    class Meta:
        model = ProductModel
        fields = "__all__"
