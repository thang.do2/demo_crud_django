from django.shortcuts import render, redirect
from demo_django.models import ProductModel
from demo_django.forms import Productforms


def listallproduct(request):
    products = ProductModel.objects.all()
    return render(request, "index.html", {'products': products})


def addproduct(request):
    if request.method == "POST":
        if request.POST.get('name') and request.POST.get(
            'description') and request.POST.get('price') and request.POST.get(
                'image'):
            product = ProductModel()
            product.name = request.POST.get('name')
            product.description = request.POST.get('description')
            product.price = request.POST.get('price')
            product.image = request.POST.get('image')
            product.save()
            return redirect('/')
    else:
        return render(request, 'addproduct.html')


def edit(request, id):
    product = ProductModel.objects.get(id=id)
    return render(request, "edit.html", {'product': product})


def update(request, id):
    updateproduct = ProductModel.objects.get(id=id)
    forms = Productforms(request.POST, instance=updateproduct)
    if forms.is_valid():
        forms.save()
        return render(request, 'edit.html', {
            'product': updateproduct})


def delete(request, id):
    product = ProductModel.objects.get(id=id)
    product.delete()
    return redirect('/')
